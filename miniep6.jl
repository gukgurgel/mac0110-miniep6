# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    first_num = 0
    for i = 1:n^3
        sum = 0
        for j = i-1:i+n-2
            sum += 2*j+1
            first_num = 2*((j-(n-2))-1)+1
        end
        if sum == n^3
            break
        end
    end
    return first_num
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
        first_num = 0
    for i = 1:n^3
        sum = 0
        for j = i-1:i+n-2
            sum += 2*j+1
            first_num = 2*((j-(n-2))-1)+1
        end
        if sum == n^3
            break
        end
    end

    print(n," ",n^3)

    for i = 0:n-1
        print(" ",first_num + 2*i)
    end
end

function mostra_n(n)
    for i = 1:n
        imprime_impares_consecutivos(i)
        println()
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()